.include "ip65/inc/common.i"
.include "ip65/inc/commonprint.i"
.include "ip65/inc/net.i"
.include "zeropage.inc"

; assembler symbols
.import cfg_mac
.import tcp_connect
.import tcp_callback
.import tcp_connect_ip
.import tcp_connect_remote_port
.import tcp_inbound_data_ptr
.import tcp_inbound_data_length
.import tcp_send
.import tcp_send_data_len
.import tcp_send_keep_alive
.import tcp_send_string

; c symbols
.import _puts
.import _exit

; we only export C symbols for now
.export _net_init
.export _net_dhcp
.export _net_poll
.export _net_tcp_connect
.export _net_tcp_send
.export _net_tcp_send_keep_alive
.export _net_tcp_send_string

; some handy aliases - see net.h
.export _net_config = cfg_mac ; struct
.export _net_tcp_callback = tcp_callback
.export _net_tcp_connect_ip = tcp_connect_ip
.export _net_tcp_connect_remote_port = tcp_connect_remote_port
.export _net_tcp_inbound_data_ptr = tcp_inbound_data_ptr
.export _net_tcp_inbound_data_length = tcp_inbound_data_length

.code
; exits on failure
_net_init:
	jsr ip65_init
	bcs :+
	rts
:	ldax #nonet
	jsr _puts
	jsr _exit

; returns A: bool
_net_dhcp:
	jsr dhcp_init
cbool:	lda #1 ; subroutine carry -> bool for C
	bcc :+
	lda #0
:	rts

_net_poll:
	jsr ip65_process
	jmp cbool

_net_tcp_connect:
	jsr tcp_connect
	jmp cbool

_net_tcp_send_string:
	jsr tcp_send_string
	jmp cbool

_net_tcp_send_keep_alive:
	jsr tcp_send_keep_alive
	jmp cbool

_net_tcp_send:
	stax tcp_send_data_len
	plax
	jsr tcp_send
	jmp cbool

.rodata
nonet:
	.asciiz "No network hardware detected, giving up"
