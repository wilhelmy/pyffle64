#ifndef _NET_H
#define _NET_H

typedef unsigned char ip_t[4];
typedef unsigned char mac_t[6];
typedef unsigned int  port_t;

struct net_config {
	mac_t     mac;
	ip_t      ip;
	ip_t      netmask;
	ip_t      gateway;
	ip_t      dns;
	ip_t      dhcp;
};

enum {
	NOT_YET,
	ESTABLISHED,
	LOST
};

void fastcall                net_init(void);
unsigned char fastcall       net_dhcp(void);
unsigned char fastcall       net_connect_tcp(struct tcp_c *);
unsigned char fastcall       net_poll(void);

unsigned char fastcall       net_tcp_send_keep_alive(void);
unsigned char fastcall       net_tcp_send_string(unsigned char *data);
unsigned char fastcall       net_tcp_send(unsigned char *data, unsigned length);
unsigned char fastcall       net_tcp_connect(port_t dst_port);

/* static memory addresses */
extern struct net_config net_config;

/* TCP stuff */
extern ip_t net_tcp_connect_ip;
extern port_t net_tcp_connect_remote_port;
extern unsigned net_tcp_callback;
extern unsigned char *net_tcp_inbound_data_ptr;
extern unsigned net_tcp_inbound_data_length;

#endif /* _NET_H */
