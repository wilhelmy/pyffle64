IP65DIRS=	ip65/drivers ip65/ip65
PRG=		client.prg
SRC=		main.c net.s ui.c charconv.c
ASSRC=		$(SRC:.c=.s)
OBJ=		$(ASSRC:.s=.o)
COMMONFLAGS=	-t c64
ASFLAGS=	$(COMMONFLAGS) --include-dir ip65/inc
CFLAGS=		$(COMMONFLAGS)
LDFLAGS=	$(COMMONFLAGS) --lib-path ip65/drivers --lib-path ip65/ip65
LIBS=		ip65_tcp.lib c64rrnet.lib
CC=		cc65
AS=		ca65
LD=		cl65
INTERMEDIATE=	$(filter-out $(SRC), $(ASSRC))
HEADERS=	$(wildcard *.h)

# Disable all other suffix rules
.SUFFIXES: .c .o .s

# Don't delete intermediate .s files after build as I might look into them
.SECONDARY: $(INTERMEDIATE)

.PHONY: $(IP65DIRS) ip65 clean-ip65 all clean

all: $(PRG)

# These targets build the ip65 git submodule
ip65: $(IP65DIRS)
prepare: $(IP65DIRS)

$(IP65DIRS):
	$(MAKE) -C $@

.c.s:
	$(CC) $(CFLAGS) $<

.s.o:
	$(AS) $(ASFLAGS) $<

# cancel this gmake implicit rule as it adds "-c" to $(CC) where it shouldn't
%.o: %.c

$(PRG): $(OBJ)
	$(LD) $(LDFLAGS) -o $@ -vm -m $@.map $(OBJ) $(LIBS)

clean:
	rm -f $(INTERMEDIATE)
	rm -f $(OBJ) $(PRG) $(PRG).map

clean-ip65:
	for x in $(IP65DIRS); do make -C $$x clean; done
