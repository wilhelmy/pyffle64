#include <conio.h>
#include <stdint.h>
#include "ui.h"
#include "net.h"

/* corners */
#define TOP_LEFT      0xB0
#define BOTTOM_LEFT   0xAD
#define TOP_RIGHT     0xAE
#define BOTTOM_RIGHT  0xBD

/* this is somewhat bloated, rewrite in asm if necessary */
static void __fastcall__ 
box(unsigned char x, unsigned char y, unsigned char w, unsigned char h, unsigned char clear)
{
	unsigned char r = x+w;
	unsigned char b = y+h;

	if (clear) { /* mostly untested, supposed to clear the box first */
		unsigned char i;
		gotoxy(x,y);
		for (i = 1; i < b;) {
			cclear(w);
			gotoy(++i);
		}
	}
		
	chlinexy(x, y, w);
	cvlinexy(x, y, h);
	cvlinexy(r, y, h);
	chlinexy(x, b, w);

	cputcxy(x,y,TOP_LEFT);
	cputcxy(r,y,TOP_RIGHT);
	cputcxy(x,b,BOTTOM_LEFT);
	cputcxy(r,b,BOTTOM_RIGHT);
}

#define LABEL(l,t) do { cputsxy(3,l,t); gotox(10); cputc(':'); } while(0)
#define XOFFS 17
#define cprintfxy(x,y,...) do { gotoxy(x,y); cprintf(__VA_ARGS__); } while(0)
#define IP(l,d)   cprintfxy(XOFFS,l,"%3u.%3u.%3u.%3u", d[0], d[1], d[2], d[3])
#define MAC(l,d)  cprintfxy(XOFFS,l,"%02x-%02x-%02x-%02x-%02x-%02x", d[0],d[1],d[2],d[3],d[4],d[5])
#define PORT(l,d) cprintfxy(XOFFS,l,"%d",d);

#define SETCOLOR()     bgcolor(9), textcolor(10)
#define RESETCOLOR()   bgcolor(15), textcolor(0)

void
ui_setup(void)
{
	bordercolor(10);
	textcolor(10);
	bgcolor(9);
}

static unsigned char c; /* current character */

unsigned char
ui_network_setup(void)
{

	net_dhcp();
	return 0;
setup:
	clrscr();
	box(2,2,35,20,0);
	gotoxy(2,1);
	cputs("Network Setup");
	gotoxy(3,22);
	cputs("F1=DONE F3=DHCP");

	LABEL(3, "MAC");
	LABEL(5, "IP");
	LABEL(7, "Netmask");
	LABEL(9, "Gateway");

	LABEL(12,"Server");
	LABEL(14,"Port");

refresh:
	revers(1);

	MAC(3, net_config.mac);
	IP(5, net_config.ip);
	IP(7, net_config.netmask);
	IP(9, net_config.gateway);

	IP(12, net_tcp_connect_ip);
	PORT(14, 6502);

	revers(0);
	/*gotoxy(XOFFS,3);
	cursor(1);*/
	
	while (c = cgetc()) {
		if (c == 134) {
			cputsxy(2,24,"Trying to get DHCP lease");
			if (net_dhcp())
				cputsxy(2,24,"DHCP succeeded          ");
			else
				cputsxy(2,24,"DHCP failed             ");
			goto refresh;
		}
		if (c == 133) {
			clrscr();
			return 1;
		}
		//gotoxy(0,24); cprintf("%d,",c);
	}
	return 1;
}

void fastcall statusbar (unsigned char) { /* TODO */ }
