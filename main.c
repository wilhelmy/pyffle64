#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "net.h"
#include "ui.h"
#include "charconv.h"

#include "password.h" /* temporarily, #define USERNAME and PASSWORD in there */

unsigned char connection = NOT_YET; /* doubles as callback return value */
unsigned char status = 0; /* connection status */
char reqbuf[64]; /* request buffer, keep the session ID in here for the next time */
unsigned char const login_string[] = "!!001\r\n"USERNAME"\r\n"PASSWORD "\r\n";
unsigned char echoreq[] = "!!999>0\r\n666\r\nHallo 123 Foobar wololo Ketchup\r\n.\r\n";
#define REQ_ST(c) sscanf("%3u", net_tcp_inbound_data_ptr, c)

#include <conio.h>
void sendstr(char *str)
{
	textcolor(3);
	puts(str);
	net_tcp_send_string(str);
	textcolor(10);
}
#define net_tcp_send_string sendstr

void tcp_cb(void)
{
	if (net_tcp_inbound_data_length == 0xffff) {
		connection = LOST;
		return;
	}

	AscToPet(net_tcp_inbound_data_ptr, net_tcp_inbound_data_ptr+net_tcp_inbound_data_length);
	/* DEBUG: print each packet as soon as it's received */
	net_tcp_inbound_data_ptr[net_tcp_inbound_data_length] = '\0';
	puts(net_tcp_inbound_data_ptr);

	switch (status) {
	case 0: /* login banner received */
		net_tcp_send_string("!!000\r\n1\r\n"); /* XXX check version number */
		++status;
		break;
	case 1: /* status reply to !!000 received */
		strcpy(reqbuf, login_string);
		PetToAsc(reqbuf, reqbuf+sizeof(login_string));
		net_tcp_send_string(reqbuf); //, sizeof(login_string));
		++status;
		break;
	case 2: /* session cookie received */
		PetToAsc(echoreq, echoreq+sizeof(echoreq));
		net_tcp_send_string(echoreq);
		//REQ_ST(&s);
		//printf("D:%d\n",s);
		status++;
		break;
	}
}

void main(void)
{
	const ip_t ip = { 5,2,18,47 };
	//const ip_t ip = {192,168,0,9 };
	net_init();
	ui_setup();

	net_tcp_connect_ip[0] =  ip[0];
	net_tcp_connect_ip[1] =  ip[1];
	net_tcp_connect_ip[2] =  ip[2];
	net_tcp_connect_ip[3] =  ip[3];
	net_tcp_connect_remote_port = 6502;
	net_tcp_callback = (unsigned)tcp_cb;
	ui_network_setup();

	if (!net_tcp_connect(net_tcp_connect_remote_port))
		puts("Connection failed."); /* XXX -> ui.c */
	else
		connection = ESTABLISHED;

	/* main loop */
	while (1) {
		net_poll(); /* check for pending network data */
		/* TODO if (connection == LOST) -> ui_reconnect() */
	}
	exit(0);
}
